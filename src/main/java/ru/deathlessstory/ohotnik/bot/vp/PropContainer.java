package ru.deathlessstory.ohotnik.bot.vp;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 21.01.14
 * Time: 22:39
 */
public class PropContainer {

    static Properties prop = null;

    private static Map<String, String> props = new HashMap<String, String>();

    static {
        prop = new Properties();
        InputStream input = null;

        try {

            String filename = "config.properties";
            input = MC.class.getClassLoader().getResourceAsStream(filename);
            if (input == null) {
                System.out.println("Sorry, unable to find " + filename);
            }

            Reader reader = new InputStreamReader(input, "UTF-8");

            // load a properties file
            prop.load(reader);

            System.out.println("Properties : ");
            System.out.println("login = " + prop.getProperty("login"));
            addProperty("login", prop.getProperty("login"));
            System.out.println("password = " + prop.getProperty("password"));
            addProperty("password", prop.getProperty("password"));
            System.out.println("world = " + prop.getProperty("world"));
            addProperty("world",  prop.getProperty("world"));
            System.out.println("server = " + prop.getProperty("server"));
            addProperty("server", prop.getProperty("server"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getProperty(String key) {
        if (props.containsKey(key)) {
            return props.get(key);
        }
        return null;
    }

    public static void addProperty(String key, String value) {
        props.put(key, value);
    }

}
