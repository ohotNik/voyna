package ru.deathlessstory.ohotnik.bot.vp.gui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.deathlessstory.ohotnik.bot.vp.browser.FFBrowser;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 23.01.14
 * Time: 21:47
 */
public class StatPanel extends JPanel {

    public static JTable table;

    public StatPanel() {
        super();
        update();
        this.add(table);
    }

    public static void update() {
        WebDriver driver = FFBrowser.getInstance();
        synchronized (driver) {
            LogPanel.addMessage("Обновляем статистику деревни...");
            table = new JTable(new MyTableModel(driver));
        }
        if (table.getParent() != null) {
            table.getParent().repaint();
        }

    }

    private static class MyTableModel extends AbstractTableModel {

        int col = 5;
        int row = 1;
        Object[][] obj = new Object[2][5];
        WebDriver driver;

        public MyTableModel(WebDriver driver) {
            this.driver = driver;
        }

        protected String[] columnNames = new String[]{
                "№", "Название", "Очки", "Координаты", "Перейти"
        };

        protected Class[] columnClasses = new Class[]{
                String.class, String.class, Integer.class,
                String.class, String.class
        };

        @Override
        public int getRowCount() {
            return row;
        }

        @Override
        public int getColumnCount() {
            return col;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            WebElement element;
            switch (columnIndex) {
                case 0:
                    return 1;
                case 1:
                    element = driver.findElement(By.id("header_info"));
                    element = element.findElements(By.tagName("a")).get(0);
                    return element.getText();
                case 3:
                    element = driver.findElement(By.id("header_info"));
                    element = element.findElement(By.tagName("b"));
                    return element.getText();
                case 4:
                    return "еще не вкл";
            }
            return 1;
        }

        // Information about each column
        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Class getColumnClass(int col) {
            return columnClasses[col];
        }

    }

}
