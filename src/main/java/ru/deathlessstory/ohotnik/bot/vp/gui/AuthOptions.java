package ru.deathlessstory.ohotnik.bot.vp.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JPanel;

import ru.deathlessstory.ohotnik.bot.vp.PropContainer;

/**
 * Created by OkhonchenkoAV on 24.01.14.
 */
public class AuthOptions extends JPanel {

	public static JEditorPane login;
	public static JEditorPane password;
	public static JEditorPane world;

	public AuthOptions() {
		super();
		login = new JEditorPane();
		login.setToolTipText("Логин");
		this.add(login);
		password = new JEditorPane();
		password.setToolTipText("Пароль");
		this.add(password);
		world = new JEditorPane();
		world.setToolTipText("Мир");
		world.setText("Мир 28");
		this.add(world);

		JButton button = new JButton("Ввести");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!login.getText().trim().isEmpty()) {
					PropContainer.addProperty("login", login.getText().trim());
				}
				if (!password.getText().trim().isEmpty()) {
					PropContainer.addProperty("password", password.getText().trim());
				}
				if (!world.getText().trim().isEmpty()) {
					PropContainer.addProperty("world", world.getText().trim());
				}
			}
		});

		this.add(button);
	}

}
