package ru.deathlessstory.ohotnik.bot.vp.browser;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 25.01.14
 * Time: 15:50
 */
public class BrowseUtils {

    public static void goToMain() {
        WebDriver driver = FFBrowser.getInstance();
        WebElement element = driver.findElement(By.id("menu_row2_village"));
        element = element.findElement(By.tagName("a"));
        element.click();
    }

    public static void goToMainBuild() {
        goToMain();
        WebDriver driver = FFBrowser.getInstance();
        WebElement element = driver.findElement(By.id("map_main"));
        element.click();
    }

}
