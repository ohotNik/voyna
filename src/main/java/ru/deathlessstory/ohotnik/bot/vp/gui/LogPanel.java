package ru.deathlessstory.ohotnik.bot.vp.gui;

import javax.swing.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 25.01.14
 * Time: 13:06
 */
public class LogPanel extends JPanel {

    private static JTextArea jTextArea = new JTextArea();

    public LogPanel() {
        super();
        jTextArea.setEditable(false);
        add(jTextArea);

    }

    public static synchronized void addMessage(String message) {
        Date date = new Date();
        jTextArea.append(date + " " + message + "\n");
    }

}
