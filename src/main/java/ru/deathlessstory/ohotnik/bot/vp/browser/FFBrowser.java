package ru.deathlessstory.ohotnik.bot.vp.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 21.01.14
 * Time: 22:33
 */
public class FFBrowser {

    private static WebDriver instance = new FirefoxDriver();

    public static WebDriver getInstance() {
        return instance;
    }

}
