package ru.deathlessstory.ohotnik.bot.vp.gui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.deathlessstory.ohotnik.bot.vp.browser.BrowseUtils;
import ru.deathlessstory.ohotnik.bot.vp.browser.FFBrowser;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 25.01.14
 * Time: 14:44
 */
public class BuildPanel extends JPanel {

    public static JTable table;

    public BuildPanel() {
        super();
        update();
        add(table);
    }

    public static void update() {
        WebDriver driver = FFBrowser.getInstance();
        synchronized (driver) {
            LogPanel.addMessage("Обновляем постройки...");
            BrowseUtils.goToMainBuild();

            table = new JTable(new MyTableModel(driver));
        }
        if (table.getParent() != null) {
            table.getParent().repaint();
        }

    }

    private static class MyTableModel extends AbstractTableModel {

        int col = 5;
        int row = 1;
        Object[][] obj = new Object[2][5];
        WebDriver driver;
        WebElement buildings;

        public MyTableModel(WebDriver driver) {
            this.driver = driver;
            buildings = driver.findElement(By.id("buildings"));
            row = buildings.findElements(By.tagName("tr")).size() - 1;
        }

        protected String[] columnNames = new String[]{
                "Постройка", "Уровень", "Ресурсы", "Время", "Строить?"
        };

        protected Class[] columnClasses = new Class[]{
                String.class, String.class, String.class,
                String.class, Boolean.class
        };

        @Override
        public int getRowCount() {
            return row;
        }

        @Override
        public int getColumnCount() {
            return col;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            WebElement element = buildings.findElements(By.tagName("tr")).get(rowIndex + 1);
            StringBuilder answer = new StringBuilder();
            boolean flag = isFullBuild(element);
            switch (columnIndex) {
                case 0:
                    return element.findElements(By.tagName("a")).get(1).getText();
                case 1:
                    return element.findElement(By.tagName("span")).getText();
                case 2:
                    if (flag) {
                        return "Построено полностью";
                    }
                    answer.append(element.findElements(By.tagName("td")).get(1).getText());
                    answer.append(" / ");
                    answer.append(element.findElements(By.tagName("td")).get(2).getText());
                    answer.append(" / ");
                    answer.append(element.findElements(By.tagName("td")).get(3).getText());
                    return answer.toString();
                case 3:
                    if (flag) {
                        return "";
                    }
                    return element.findElements(By.tagName("td")).get(4).getText();
                case 4:
                    JCheckBox jCheckBox = new JCheckBox();
                    if (flag) {
                        jCheckBox.setEnabled(false);
                    }
                    return jCheckBox.isSelected();
            }
            return 1;
        }

        private boolean isFullBuild(WebElement element) {
            try {
                Integer.parseInt(element.findElements(By.tagName("td")).get(1).getText());
            } catch (Exception e) {
                return true;
            }
            return false;
        }

        // Information about each column
        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Class getColumnClass(int col) {
            return columnClasses[col];
        }

    }

}
