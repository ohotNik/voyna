package ru.deathlessstory.ohotnik.bot.vp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ru.deathlessstory.ohotnik.bot.vp.browser.FFBrowser;
import ru.deathlessstory.ohotnik.bot.vp.gui.LogPanel;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

/**
 * Created by OkhonchenkoAV on 24.01.14.
 */
public class ResoPanel extends JPanel {

    public static JTable table;

    public ResoPanel() {
        super();
        update();
        this.add(table);
    }

    public static void update() {
        WebDriver driver = FFBrowser.getInstance();
        synchronized (driver) {
            LogPanel.addMessage("Обновляем ресурсы...");
            table = new JTable(new MyTableModel(driver));
        }
        if (table.getParent() != null) {
            table.getParent().repaint();
        }

    }

    private static class MyTableModel extends AbstractTableModel {

        int col = 5;
        int row = 1;
        Object[][] obj = new Object[2][5];
        WebDriver driver;

        public MyTableModel(WebDriver driver) {
            this.driver = driver;
        }

        protected String[] columnNames = new String[]{
                "Деревня", "Дерево", "Глина", "Железо", "Склад"
        };

        protected Class[] columnClasses = new Class[]{
                String.class, String.class, String.class,
                String.class, Integer.class
        };

        @Override
        public int getRowCount() {
            return row;
        }

        @Override
        public int getColumnCount() {
            return col;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            StringBuilder answer = new StringBuilder();
            switch (columnIndex) {
                case 0:
                    return 1;
                case 1:
                    answer.append(driver.findElement(By.id("wood")).getText());
                    answer.append(" // " + getResoGrow("wood"));
                    return answer.toString();
                case 2:
                    answer.append(driver.findElement(By.id("stone")).getText());
                    answer.append(" // " + getResoGrow("stone"));
                    return answer.toString();
                case 3:
                    answer.append(driver.findElement(By.id("iron")).getText());
                    answer.append(" // " + getResoGrow("iron"));
                    return answer.toString();
                case 4:
                    answer.append(driver.findElement(By.id("storage")).getText());
                    return answer.toString();
            }
            return 1;
        }

        private int getResoGrow(String resoId) {
            String str = driver.findElement(By.id(resoId)).getAttribute("title");
            str = str.replaceAll("^\\W+", "");
            str = str.replaceAll("\\W+$", "");
            return Integer.parseInt(str);
        }

        // Information about each column
        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Class getColumnClass(int col) {
            return columnClasses[col];
        }

    }

}
