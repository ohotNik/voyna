package ru.deathlessstory.ohotnik.bot.vp.gui;

import ru.deathlessstory.ohotnik.bot.vp.ResoPanel;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 23.01.14
 * Time: 21:46
 */
public class MyTabbedPane extends JTabbedPane {

    public MyTabbedPane() {
        super();
        this.add("Данные", new StatPanel());
        this.add("Авторизация", new AuthOptions());
        this.setEnabledAt(1, false);
        this.add("Ресурсы", new ResoPanel());
        this.add("Лог", new LogPanel());
        this.add("Постройки", new BuildPanel());
        this.add("Настройки", new JPanel());
        this.add("Почта", new PostPanel());
    }

}
