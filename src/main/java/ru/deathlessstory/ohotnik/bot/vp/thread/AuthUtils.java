package ru.deathlessstory.ohotnik.bot.vp.thread;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.deathlessstory.ohotnik.bot.vp.PropContainer;
import ru.deathlessstory.ohotnik.bot.vp.browser.FFBrowser;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 21.01.14
 * Time: 22:38
 */
public class AuthUtils {

    public static void execute() {
        String server = PropContainer.getProperty("server");
        WebDriver browser = FFBrowser.getInstance();
        browser.get(server);

        String login = PropContainer.getProperty("login");
        String password = PropContainer.getProperty("password");

        WebElement element = browser.findElement(By.id("user"));
        element.sendKeys(login);
        element = browser.findElement(By.id("password"));
        element.sendKeys(password);
        element = browser.findElement(By.xpath("//span[@class='button_middle']"));
        element.click();
        String world = PropContainer.getProperty("world");

        element = (new WebDriverWait(browser, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='" + world + "']")));
        element.click();
    }

}
