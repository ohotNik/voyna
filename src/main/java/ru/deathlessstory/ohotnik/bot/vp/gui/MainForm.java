package ru.deathlessstory.ohotnik.bot.vp.gui;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: ohotNik
 * Date: 23.01.14
 * Time: 21:42
 */
public class MainForm extends JFrame {

    public MainForm() {
        super();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(800, 600);
        this.setVisible(true);
        this.setTitle("VP_BOT 0.1-alpha");
        this.add(new MyTabbedPane());
    }

}
